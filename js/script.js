"use strict";

// Теоретичні питання:
// 1. var імяЗмінної  (застаріле, не рекомендується використовувати);
// var імяЗмінної (те, що рекомендується використовувати);
// const ІМЯ_ЗМІННОЇ (точніше це не змінна, а стала, так як її значення ми не можемо поміняти, воно залишається сталим впродовж проекту);

// 2 String - це змінна, яка має нецифрове значення.
// Варіанти створення: let name = "Pedro"; let name = 'Pedros'; let name `Pedricio`;

// 3 ввести console.log(typeof name);  ------> 
// ------> в браузері в консолі побачимо або прості типи даних: number, string, undefined,
// або boolean: true, false
// або null (порожній обєкт), обєкти (обєкти, масиви, функції (тобто структури)),
//symbol (символ) – доступний з ES2015, bigint (велике ціле) – доступний з ES2020;

// 4 оператор '+' виконує конкатенацію, тобто склеює 2 рядки. Так як '1' - це стрінг, то результат теж буде string, навіть якщо інший елемент - number;

//Практичне завдання:

//1 
let age;
console.log(age); //undefined
age = 30;
console.log(typeof age); //number
console.log(age); //30

//2 
let name1 = 'Not Jane';
let lastName1 = 'Cooper';
console.log(`Мене звати ${name1} ${lastName1}`);
//or
let person = {
    name2: 'Not Jane',
    lastName2: 'Cooper'
};
console.log(`Мене звати ${person.name2} ${person.lastName2}`);

//3
let age1 = 30;
console.log(`${age1}`);
